PREFIX=/usr/local
BINDIR=${PREFIX}/bin
MANDIR=${PREFIX}/share/man/man6

all:
	@gzip -c nbmystere.6 > nbmystere.6.gz

install:
	@mkdir -p ${BINDIR} && install nbmystere ${BINDIR}
	@mkdir -p ${MANDIR} && cp nbmystere.6.gz ${MANDIR}

clean:
	@rm -f nbmystere.6.gz

uninstall:
	@rm -f ${BINDIR}/nbmystere
	@rm -f ${MANDIR}/nbmystere.6.gz
